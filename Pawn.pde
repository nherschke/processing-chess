class Pawn {
  PVector pos;
  
  Pawn(PVector pos) {
    this.pos = pos;
  }
  
  void show() {
    ellipse(pos.x, pos.y-10, 10, 10);
    triangle(pos.x-10, pos.y+10, pos.x, pos.y-10, pos.x+10, pos.y+10);
  }
  
  void moveDown(int range) {
    if (pos.y < height-range) pos.y += range;
  }
  
  void moveUp(int range) {
    if (pos.y > range) pos.y -= range;
  }
}
