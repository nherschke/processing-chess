int rows = 8;
int squareSize;
PVector pPos;
PVector p2Pos;
Pawn p;
Pawn p2;

void setup() {
  size(400, 400);
  squareSize = width/8;
  noStroke();
  pPos = new PVector(25, 75);
  p2Pos = new PVector(375, 325);
  p = new Pawn(pPos);
  p2 = new Pawn(p2Pos);
}

void draw() {
  drawBoard(rows, squareSize);
  stroke(0);
  fill(100);
  p.show();
  fill(255);
  p2.show();
}

void keyPressed() {
  if (keyCode == DOWN) p.moveDown(squareSize);
  if (keyCode == UP) p2.moveUp(squareSize);
}

void drawBoard(int r, int s) {
  for (int i = 0; i < r; i++) {
    for (int j = 0; j < r; j++) {
      if ((i+j)%2 == 0) fill(255);
      else fill(0);
      rect(i*s, j*s, s, s);
    }
  }
}
